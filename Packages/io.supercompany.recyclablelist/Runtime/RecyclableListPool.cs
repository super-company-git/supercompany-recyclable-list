using Supercompany.Core;
using UnityEngine;

namespace Supercompany.RecyclableList
{
    public class RecyclableListPool : MonoBehaviour
    {
        public event System.Action<IRecyclableCell> OnTakeFromPoolEvent;
        public event System.Action<IRecyclableCell> OnReturnToPoolEvent;

        private ObjectPool<IRecyclableCell> _pool;
        private Vector2? _anchorMin = null, _anchorMax = null;

        public bool IsInitialized { get; private set; }
        public IRecyclableCell CellPrefab { get; set; }

        public void Initialize(int defaultCapacity, IRecyclableCell cellPrefab, Vector2? anchorMin = null, Vector2? anchorMax = null)
        {
            CellPrefab = cellPrefab;
            _anchorMin = anchorMin;
            _anchorMax = anchorMax;
            _pool = new ObjectPool<IRecyclableCell>(CreatePooledItem, OnTakeFromPool, OnReturnedToPool, defaultCapacity: defaultCapacity, initialSize: defaultCapacity);

            IsInitialized = true;
        }

        private IRecyclableCell CreatePooledItem()
        {
            RectTransform cellTransform = Instantiate(CellPrefab.RectTransform, transform);

            if (_anchorMin.HasValue && _anchorMax.HasValue)
            {
                cellTransform.anchorMin = _anchorMin.Value;
                cellTransform.anchorMax = _anchorMax.Value;
            }

            IRecyclableCell cell = cellTransform.GetComponent<IRecyclableCell>();

            return cell;
        }

        private void OnTakeFromPool(IRecyclableCell cell)
        {
            cell.RectTransform.gameObject.SetActive(true);
            OnTakeFromPoolEvent?.Invoke(cell);
        }

        private void OnReturnedToPool(IRecyclableCell cell)
        {
            cell.RectTransform.gameObject.SetActive(false);
            OnReturnToPoolEvent?.Invoke(cell);
        }

        public IRecyclableCell GetCell()
        {
            return _pool.Get();
        }

        public void ReleaseCell(IRecyclableCell cell)
        {
            _pool.Release(cell);
        }
    }
}
