using UnityEngine;

namespace Supercompany.RecyclableList
{
    public interface IRecyclableCell
    {
        int Index { get; set; }
        RectTransform RectTransform { get; }
        void OnShow();
        void OnHide();
    }
}
