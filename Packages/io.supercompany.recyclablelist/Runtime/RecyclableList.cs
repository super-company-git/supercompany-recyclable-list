using Supercompany.Core;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Supercompany.RecyclableList
{
    [RequireComponent(typeof(RectTransform))]
    public class RecyclableList : MonoBehaviour
    {
        [System.Serializable]
        private struct Padding
        {
            public float Left;
            public float Right;
            public float Top;
            public float Bottom;
        }

        public enum Direction { Vertical, Horizontal }

        [SerializeField] private Direction _direction = Direction.Vertical;
        [SerializeField] private bool _forceExpand = false;
        [SerializeField] private bool _keepAspectRatioOnForceExpand = true;
        [SerializeField] private bool _autoCreatePool = true;
        [SerializeField] private bool _autoInitializePool = true;
        [SerializeField] private RecyclableListPool m_pool;
        [SerializeField] private GameObject _cellPrefab;
        [SerializeField] private ScrollRect _scrollRect;

        [Header("Padding and spacing")]
        [SerializeField] private Padding _padding;
        [SerializeField] private float _spacing;

        private bool _isInitialized = false;
        private int _firstVisibleCellIndex;
        private int _lastVisibleCellIndex;
        private Vector2 _defaultCellSize;
        private readonly Dictionary<int, IRecyclableCell> _activeCells = new();
        private readonly SortedDictionary<int, Vector2> _customSizedCells = new();

        private RectTransform m_RectTransform;
        private RectTransform RectTransform
        {
            get
            {
                if (m_RectTransform == null)
                    m_RectTransform = GetComponent<RectTransform>();
                return m_RectTransform;
            }
        }

        public RecyclableListPool CurrentPool { get; set; }
        public IRecyclableListDataSource DataSource { get; set; }
        public IEnumerable<IRecyclableCell> ActiveCells => _activeCells.Values;
        public Dictionary<int, IRecyclableCell> ActiveCellsDictionary => _activeCells;
        public SortedDictionary<int, Vector2> CustomSizedCells => _customSizedCells;
        private Padding _additionalPadding; 

        public void Initialize(IRecyclableListDataSource dataSource)
        {
            StartCoroutine(InitializeRoutine(dataSource));
        }

        private IEnumerator InitializeRoutine(IRecyclableListDataSource dataSource)
        {
            if (!_cellPrefab.TryGetComponent(out IRecyclableCell cell))
            {
                Debug.LogError("Cell prefab must implement IRecyclableCell!");
                yield break;
            }

            RectTransform cellTransform = cell.RectTransform;

            if (cellTransform == null)
            {
                Debug.LogError("Cell must have a RectTransform component!");
                yield break;
            }

            if (m_pool != null)
                CurrentPool = m_pool;
            else if (_autoCreatePool)
                CurrentPool = gameObject.AddComponent<RecyclableListPool>();
            else if (CurrentPool == null)
            {
                Debug.LogError("Pool cannot be null!");
                yield break;
            }

            if (_forceExpand)
            {
                while (RectTransform.rect.size.HasZeros())
                    yield return null;

                float aspectRatio = cellTransform.sizeDelta.x / cellTransform.sizeDelta.y;

                if (_direction == Direction.Vertical)
                    _defaultCellSize = new Vector2(RectTransform.rect.size.x, _keepAspectRatioOnForceExpand ? RectTransform.rect.size.x / aspectRatio : cellTransform.sizeDelta.y);
                else
                    _defaultCellSize = new Vector2(_keepAspectRatioOnForceExpand ? RectTransform.rect.size.y * aspectRatio : RectTransform.rect.size.y, RectTransform.rect.size.y);
            }
            else
                _defaultCellSize = cellTransform.sizeDelta;

            while (_scrollRect.viewport.rect.size.HasZeros())
                yield return null;

            if (_autoInitializePool && !CurrentPool.IsInitialized)
            {
                int maxVisibleCells = Mathf.CeilToInt(GetAxis(_scrollRect.viewport.rect.size) / (GetAxis(_defaultCellSize) + _spacing)) + 1;

                Vector2 anchor = _direction == Direction.Vertical ? new Vector2(0.5f, 1f) : new Vector2(0f, 0.5f);
                CurrentPool.Initialize(maxVisibleCells, cell, anchor, anchor);
            }

            _isInitialized = true;

            _scrollRect.onValueChanged.AddListener(OnScrollRectMove);

            SetDataSource(dataSource);
        }

        public void SetDataSource(IRecyclableListDataSource dataSource)
        {
            if (!_isInitialized)
            {
                Initialize(dataSource);
                return;
            }

            int[] cellIndexes = _activeCells.Keys.ToArray();
            foreach (int index in cellIndexes)
                TryHideCellAtIndex(index);

            _activeCells.Clear();

            DataSource = dataSource;

            _firstVisibleCellIndex = -1;
            _lastVisibleCellIndex = -1;

            _customSizedCells.Clear();

            UpdateContentSize();
            UpdateView();
        }

        private void OnDestroy()
        {
            _scrollRect.onValueChanged.RemoveListener(OnScrollRectMove);
        }

        public void OnElementSizeChanged(int index, Vector2 size)
        {
            if (size == _defaultCellSize)
                _customSizedCells.Remove(index);
            else
                _customSizedCells[index] = size;

            if (_activeCells.TryGetValue(index, out IRecyclableCell cell))
                cell.RectTransform.sizeDelta = GetElementSize(index);

            foreach (var recyclableCell in _activeCells)
                recyclableCell.Value.RectTransform.anchoredPosition = GetElementPosition(recyclableCell.Key, recyclableCell.Value.RectTransform);

            UpdateContentSize();
            UpdateView();
        }

        private void OnScrollRectMove(Vector2 position)
        {
            UpdateView();
        }

        public void UpdateView()
        {
            if (!_isInitialized)
                return;

            int previousFirstVisibleCellIndex = _firstVisibleCellIndex;
            int previousLastVisibleCellIndex = _lastVisibleCellIndex;

            (_firstVisibleCellIndex, _lastVisibleCellIndex) = GetFirstAndLastVisibleCellIndexes();

            if (_firstVisibleCellIndex > previousFirstVisibleCellIndex)
            {
                for (int i = previousFirstVisibleCellIndex; i < _firstVisibleCellIndex; i++)
                {
                    TryHideCellAtIndex(i);
                }
            }
            else if (_firstVisibleCellIndex < previousFirstVisibleCellIndex)
            {
                for (int i = Mathf.Min(previousFirstVisibleCellIndex - 1, _lastVisibleCellIndex); i >= _firstVisibleCellIndex; i--)
                {
                    TryShowCellAtIndex(i);
                }
            }

            if (_lastVisibleCellIndex > previousLastVisibleCellIndex)
            {
                for (int i = Mathf.Max(previousLastVisibleCellIndex + 1, _firstVisibleCellIndex); i <= _lastVisibleCellIndex; i++)
                {
                    TryShowCellAtIndex(i);
                }
            }
            else if (_lastVisibleCellIndex < previousLastVisibleCellIndex)
            {
                for (int i = previousLastVisibleCellIndex; i > _lastVisibleCellIndex; i--)
                {
                    TryHideCellAtIndex(i);
                }
            }
        }

        private bool TryShowCellAtIndex(int index)
        {
            if (index >= 0 && index < DataSource.GetItemCount())
            {
                if (!_activeCells.ContainsKey(index))
                {
                    IRecyclableCell cell = CurrentPool.GetCell();
                    _activeCells.Add(index, cell);
                    cell.Index = index;
                    cell.OnShow();
                    cell.RectTransform.sizeDelta = GetElementSize(index);
                    cell.RectTransform.anchoredPosition = GetElementPosition(index, cell.RectTransform);
                    DataSource.ShowCell(cell, index);
                    return true;
                }
            }

            return false;
        }

        private bool TryHideCellAtIndex(int index)
        {
            if (_activeCells.Remove(index, out IRecyclableCell cell))
            {
                CurrentPool.ReleaseCell(cell);
                cell.OnHide();
                DataSource.HideCell(cell, index);
                return true;
            }

            return false;
        }

        private Vector2 GetElementPosition(int index, RectTransform rectTransform = null, Vector2? customPivot = null)
        {
            if (rectTransform == null)
                rectTransform = _cellPrefab.GetComponent<RectTransform>();

            float pivotOffset = GetComplement(customPivot.HasValue ? GetAxis(customPivot.Value) : GetAxis(rectTransform.pivot)) * GetAxis(rectTransform.sizeDelta);
            float position = GetPaddingStart() + pivotOffset + GetAxis(_defaultCellSize) * index + _spacing * index;

            foreach (var pair in _customSizedCells)
            {
                if (pair.Key < index)
                    position += GetAxis(pair.Value) - GetAxis(_defaultCellSize);
            }

            return _direction == Direction.Vertical ?
                rectTransform.anchoredPosition.WithY(position * -1f) :
                rectTransform.anchoredPosition.WithX(position);
        }

        private Vector2 GetElementSize(int index)
        {
            if (_customSizedCells.TryGetValue(index, out Vector2 size))
                return size;

            return _defaultCellSize;
        }

        private (int firstCellIndex, int lastCellIndex) GetFirstAndLastVisibleCellIndexes()
        {
            float anchorMin = GetAxis(_scrollRect.viewport.rect) * GetComplement(GetAxis(_scrollRect.content.anchorMin)) * -1f;
            float anchorMax = GetAxis(_scrollRect.viewport.rect) * GetComplement(GetAxis(_scrollRect.content.anchorMax)) * -1f;
            float pivot = GetAxis(_scrollRect.content.rect) * GetComplement(GetAxis(_scrollRect.content.pivot)) * -1f;
            float weightedAverage = Mathf.LerpUnclamped(anchorMin, anchorMax, GetAxis(_scrollRect.content.pivot));

            float viewStartPos = (((pivot - weightedAverage) * -1f) + GetAxis(_scrollRect.content.anchoredPosition)) * GetDirection() * -1f;
            float viewEndPos = viewStartPos + GetAxis(_scrollRect.viewport.rect);

            /*float firstVisibleCell = (viewStartPos - _padding.Top) / (_defaultCellSize.y + _spacing);
            float lastVisibleCell = (viewEndPos - _padding.Top + _spacing) / (_defaultCellSize.y + _spacing);

            foreach (var pair in _customSizedCells)
            {
                if (pair.Key < Mathf.FloorToInt(firstVisibleCell))
                    firstVisibleCell += 1f - ((pair.Value.y + _spacing) / (_defaultCellSize.y + _spacing));
            }

            int firstVisibleCellIndex = Mathf.FloorToInt(firstVisibleCell);
            int lastVisibleCellIndex = Mathf.CeilToInt(lastVisibleCell);

            foreach (var pair in _customSizedCells)
            {
                if (pair.Key < firstVisibleCellIndex && (GetElementPosition(pair.Key).y * -1f) + GetElementSize(pair.Key).y >= viewStartPos)
                    firstVisibleCellIndex = pair.Key;
            }*/

            float position = GetPaddingStart() + GetStartPosition();
            int firstVisibleCellIndex, lastVisibleCellIndex;

            for (firstVisibleCellIndex = 0; firstVisibleCellIndex < DataSource.GetItemCount() && position < viewStartPos; firstVisibleCellIndex++)
            {
                position += GetAxis(GetElementSize(firstVisibleCellIndex)) + _spacing;
            }

            position = GetStartPosition() + GetAxis(GetElementPosition(firstVisibleCellIndex, customPivot: _direction == Direction.Vertical ? new Vector2(0.5f, 1f) : new Vector2(0f, 0.5f))) * GetDirection();

            for (lastVisibleCellIndex = firstVisibleCellIndex; lastVisibleCellIndex < DataSource.GetItemCount() && position + GetAxis(GetElementSize(lastVisibleCellIndex)) < viewEndPos; lastVisibleCellIndex++)
            {
                position += GetAxis(GetElementSize(lastVisibleCellIndex)) + _spacing;
            }

            firstVisibleCellIndex--;

            return (firstVisibleCellIndex, lastVisibleCellIndex);
        }

        /// <summary>
        /// Update the size delta of the object containing the list.
        /// This is called when setting the data source of the list and when the size of one element in the list changes.
        /// </summary>
        private void UpdateContentSize()
        {
            if (!_isInitialized)
                return;

            float sizeDelta = GetPaddingStart() + GetPaddingEnd() + GetAxis(_defaultCellSize) * DataSource.GetItemCount() + _spacing * Mathf.Max(DataSource.GetItemCount() - 1, 0);

            foreach (Vector2 size in _customSizedCells.Values)
                sizeDelta += GetAxis(size) - GetAxis(_defaultCellSize);

            float anchorSize = GetAxis(_scrollRect.viewport.rect) * (GetAxis(RectTransform.anchorMax) - GetAxis(RectTransform.anchorMin));

            if (sizeDelta > anchorSize)
            {
                RectTransform.sizeDelta = _direction == Direction.Vertical ?
                    RectTransform.sizeDelta.WithY(sizeDelta - anchorSize) :
                    RectTransform.sizeDelta.WithX(sizeDelta - anchorSize);
            }

            LayoutRebuilder.ForceRebuildLayoutImmediate(_scrollRect.content);
        }

        private float GetAxis(Vector2 v)
        {
            return _direction == Direction.Vertical ? v.y : v.x;
        }

        private float GetAxis(Rect rect)
        {
            return _direction == Direction.Vertical ? rect.height : rect.width;
        }

        private float GetStartPosition()
        {
            if (RectTransform == _scrollRect.content)
                return 0f;

            RectTransform parent = (RectTransform)transform.parent;

            float anchorMin = GetAxis(parent.rect) * GetComplement(GetAxis(RectTransform.anchorMin)) * -1f;
            float anchorMax = GetAxis(parent.rect) * GetComplement(GetAxis(RectTransform.anchorMax)) * -1f;
            float pivot = GetAxis(RectTransform.rect) * GetComplement(GetAxis(RectTransform.pivot)) * -1f;
            float weightedAverage = Mathf.LerpUnclamped(anchorMin, anchorMax, GetAxis(RectTransform.pivot));

            float viewStartPos = (((pivot - weightedAverage) * -1f) + GetAxis(RectTransform.anchoredPosition)) * GetDirection() * -1f;

            return Mathf.Abs(viewStartPos);
        }

        private float GetPaddingStart()
        {

            return _direction == Direction.Vertical ? _padding.Top + _additionalPadding.Top: _padding.Left + _additionalPadding.Left;
        }

        private float GetPaddingEnd()
        {
            return _direction == Direction.Vertical ? _padding.Bottom + _additionalPadding.Left : _padding.Right + _additionalPadding.Right;
        }

        private float GetDirection()
        {
            return _direction == Direction.Vertical ? -1f : 1f;
        }

        private float GetComplement(float n)
        {
            return _direction == Direction.Vertical ? 1f - n : n;
        }

        public void SetAdditionalPadding(float top, float bottom, float left, float right)
        {
            _additionalPadding.Top = top;
            _additionalPadding.Bottom = bottom;
            _additionalPadding.Left = left;
            _additionalPadding.Right = right;
        }
    }
}
