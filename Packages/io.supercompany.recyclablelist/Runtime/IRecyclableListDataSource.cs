namespace Supercompany.RecyclableList
{
    public interface IRecyclableListDataSource
    {
        int GetItemCount();
        void ShowCell(IRecyclableCell cell, int index);
        void HideCell(IRecyclableCell cell, int index);
    }
}
