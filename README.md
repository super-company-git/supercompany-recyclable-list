# Supercompany Recyclable List
A recyclable list system using uGUI.


## Installing
First, install [GitDependencyResolverForUnity](https://github.com/mob-sakai/GitDependencyResolverForUnity) in your project.

Then install this package via [Unity Package Manager](https://docs.unity3d.com/Manual/upm-ui-giturl.html)
using the following URL:

```
https://gitlab.com/super-company-git/supercompany-recyclable-list.git?path=/Packages/io.supercompany.recyclablelist#1.0.0-preview5
```

## How to use
1. Create a Canvas with a Scroll View.
2. In the Content object, or in a child of the Content object, add a `RecyclableList` component.
3. In the `RecyclableList` component, set the Cell Prefab. Note that the Cell must contain a `RectTransform` component and a script component that implements the `IRecyclableCell` interface.
4. Set the Scroll Rect in the `RecyclableList` component. Also set whether your list is horizontal or vertical.
5. Create a list Data Source by implementing the `IRecyclableListDataSource` interface. You can find an example of this implementation in the `RecyclableListDataSource` class inside the Demo folder.
6. Enjoy 🍾

## Limitations
The `RecyclableList` component **must** be either the Content of the scroll rect itself, or a child of the content. It cannot be the child of a child of the content object.
