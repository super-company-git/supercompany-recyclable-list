using Supercompany.RecyclableList;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
public class RecyclableCell : MonoBehaviour, IRecyclableCell
{
    public enum ButtonType
    {
        Big,
        Medium,
        Small,
    }

    public event System.Action<ButtonType, RecyclableCell> OnClickedEvent;

    public Image Image;
    public Button BigButton;
    public Button MediumButton;
    public Button SmallButton;

    public int Index { get; set; }

    private RectTransform m_RectTransform;
    public RectTransform RectTransform
    {
        get
        {
            if (m_RectTransform == null)
                m_RectTransform = GetComponent<RectTransform>();

            return m_RectTransform;
        }
    }

    public void OnShow()
    {
        Debug.Log($"Cell {Index} is being displayed");
    }

    public void OnHide()
    {
        Debug.Log($"Cell {Index} is hidden");
    }

    public void BigButtonClicked()
    {
        OnClickedEvent?.Invoke(ButtonType.Big, this);
    }

    public void MediumButtonClicked()
    {
        OnClickedEvent?.Invoke(ButtonType.Medium, this);
    }

    public void SmallButtonClicked()
    {
        OnClickedEvent?.Invoke(ButtonType.Small, this);
    }
}
