using Supercompany.RecyclableList;
using System.Collections;
using UnityEngine;

public class RecyclableListDataSource : MonoBehaviour, IRecyclableListDataSource
{
    [SerializeField] private RecyclableList _recyclableList;
    [SerializeField] private Color[] _itemColors;
    [SerializeField] private float _bigSize;
    [SerializeField] private float _mediumSize;
    [SerializeField] private float _smallSize;
    [SerializeField] private float _growSpeed;
    [SerializeField] private RecyclableList.Direction _direction;

    private void OnEnable()
    {
        _recyclableList.SetDataSource(this);
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    public int GetItemCount()
    {
        return _itemColors.Length;
    }

    public void ShowCell(IRecyclableCell cell, int index)
    {
        RecyclableCell recyclableCell = cell as RecyclableCell;
        recyclableCell.Image.color = _itemColors[index];
        recyclableCell.OnClickedEvent += OnClickedEvent;
    }

    public void HideCell(IRecyclableCell cell, int index)
    {
        RecyclableCell recyclableCell = cell as RecyclableCell;
        recyclableCell.OnClickedEvent -= OnClickedEvent;
    }

    private void OnClickedEvent(RecyclableCell.ButtonType type, RecyclableCell cell)
    {
        if (!gameObject.activeSelf)
            return;

        if (type == RecyclableCell.ButtonType.Big)
            BigButton(cell);
        else if (type == RecyclableCell.ButtonType.Medium)
            MediumButton(cell);
        if (type == RecyclableCell.ButtonType.Small)
            SmallButton(cell);
    }

    private void BigButton(RecyclableCell cell)
    {
        StartCoroutine(ChangeSize(cell, _bigSize));
    }

    private void MediumButton(RecyclableCell cell)
    {
        StartCoroutine(ChangeSize(cell, _mediumSize));
    }

    private void SmallButton(RecyclableCell cell)
    {
        StartCoroutine(ChangeSize(cell, _smallSize));
    }

    private IEnumerator ChangeSize(RecyclableCell cell, float targetSize)
    {
        int index = cell.Index;
        float fixedSize = _direction == RecyclableList.Direction.Vertical ? cell.RectTransform.sizeDelta.x : cell.RectTransform.sizeDelta.y;
        float originalSize = _direction == RecyclableList.Direction.Vertical ? cell.RectTransform.sizeDelta.y : cell.RectTransform.sizeDelta.x;

        float t = 0f;

        while (t < 1f)
        {
            t += Time.deltaTime * _growSpeed;
            float variableSize = Mathf.Lerp(originalSize, targetSize, t);
            yield return null;
            _recyclableList.OnElementSizeChanged(index, new Vector2(_direction == RecyclableList.Direction.Vertical ? fixedSize : variableSize, _direction == RecyclableList.Direction.Vertical ? variableSize : fixedSize));
        }
    }
}
