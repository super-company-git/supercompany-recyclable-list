using UnityEngine;

public class RecyclableListController : MonoBehaviour
{
    [SerializeField] private RecyclableListDataSource[] _dataSources;

    private int _currentDataSource;

    private void Start()
    {
        _dataSources[0].gameObject.SetActive(true);
    }

    public void ChangeDataSource()
    {
        _dataSources[_currentDataSource].gameObject.SetActive(false);

        _currentDataSource++;

        if (_currentDataSource >= _dataSources.Length)
            _currentDataSource = 0;

        _dataSources[_currentDataSource].gameObject.SetActive(true);
    }
}
